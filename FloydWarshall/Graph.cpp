#include <iostream>
#include <vector>
#include <fstream>
#include "Graph.h"

int min(int a, int b) {
    return (a < b) ? a : b;
}

Graph::Graph(std::ifstream& fin) : nodeCount(0) {
    fin >> nodeCount;
    int bufInt = 0;
    for (int i = 0; i < nodeCount; i++) {
        std::vector<int> bufVec;
        table.push_back(bufVec);
        for (int j = 0; j < nodeCount; j++) {
            fin >> bufInt;
            table[i].push_back(bufInt);
        }
    }
}

Graph::Graph(std::vector< std::vector<int> > arg) {
    nodeCount = (unsigned int) arg.size();
    table = arg;
}

Graph::Graph(const Graph& g) {
    nodeCount = g.nodeCount;
    table = g.table;
}

Graph::~Graph() {}
int Graph::getNodeCount() {
    return nodeCount;
}
Graph& Graph::operator= (const Graph& g) {
    nodeCount = g.nodeCount;
    table = g.table;
    return *this;
}

FloydWarshall::FloydWarshall(Graph& object) : g(&object) {}

FloydWarshall::~FloydWarshall() {}

std::vector<int> FloydWarshall::run() {
    std::vector<int> result;
    std::vector< std::vector<int> > minDistanceTable;
    minDistanceTable = g->table;
    int n = g->nodeCount;

    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                minDistanceTable[i][j] = min(minDistanceTable[i][j], minDistanceTable[i][k] + minDistanceTable[k][j]);
            }
        }
    }

    std::ofstream fout("floyd.out");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            fout << minDistanceTable[i][j] << " ";
            result.push_back(minDistanceTable[i][j]);
        }
        fout << std::endl;
    }
    fout.close();
    return result;
}