#include "Graph.h"
#include <gtest/gtest.h>

class UnitTest : public ::testing::Test {
protected:
    Graph * object;
    FloydWarshall * solutor;

    void SetUp() {
        std::vector< std::vector<int> > arg;
        std::vector<int> bufVec = {0, 5, 9, 100};
        arg.push_back(bufVec);
        bufVec = {100, 0, 2, 8};
        arg.push_back(bufVec);
        bufVec = {100, 100, 0, 7};
        arg.push_back(bufVec);
        bufVec = {4, 100, 100, 0};
        arg.push_back(bufVec);

        object = new Graph(arg);
        solutor = new FloydWarshall(* object);
    }

    void TearDown() {;
        delete solutor;
        delete object;
    }
};

TEST_F(UnitTest, checkSolvable_1) {
    std::vector<int> buf = {0, 5, 7, 13, 12, 0, 2, 8, 11, 16, 0, 7, 4, 9, 11, 0};
    ASSERT_EQ(solutor->run(), buf);
}

TEST_F(UnitTest, checkSolvable_2) {
    std::vector< std::vector<int> > arg;
    std::vector<int> bufVec = {0, 1, 100};
    arg.push_back(bufVec);
    bufVec = {1, 0, 1};
    arg.push_back(bufVec);
    bufVec = {1, 1, 0};
    arg.push_back(bufVec);

    delete object;
    delete solutor;
    object = new Graph(arg);
    solutor = new FloydWarshall(* object);

    std::vector<int> buf = {0, 1, 2, 1, 0, 1, 1, 1, 0};
    ASSERT_EQ(solutor->run(), buf);
}

TEST_F(UnitTest, checkSolvable_3) {
    std::vector< std::vector<int> > arg;
    std::vector<int> bufVec = {0, 1};
    arg.push_back(bufVec);
    bufVec = {0, 0};
    arg.push_back(bufVec);

    delete object;
    delete solutor;
    object = new Graph(arg);
    solutor = new FloydWarshall(* object);

    std::vector<int> buf = {0, 1, 0, 0};
    ASSERT_EQ(solutor->run(), buf);
}


int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}