#pragma once
#include <vector>
#include <fstream>

int min(int a, int b);

class Graph {
    int nodeCount;
    std::vector< std::vector<int> > table;
public:
    Graph(std::ifstream& fin);
    Graph(const Graph& g);
    Graph(std::vector< std::vector<int> > arg);
    ~Graph();
    int getNodeCount();
    Graph& operator= (const Graph& g);
    friend class FloydWarshall;
};

class FloydWarshall {
    Graph *g;
public:
    FloydWarshall(Graph& object);
    ~FloydWarshall();
    std::vector<int> run();
};