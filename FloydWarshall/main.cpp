//
// Created by mikhail on 09.12.16.
//

#include "Graph.h"

int main() {
    std::ifstream fin("floyd.in");
    Graph g(fin);
    FloydWarshall f(g);
    std::ofstream fout("floyd.out");
    f.run();
    fin.close();
    fout.close();
    return 0;
}
