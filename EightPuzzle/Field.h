#pragma once
#include <vector>
#include <string>
#include <fstream>

class Field {
    unsigned short table[3][3];
    unsigned int totalEstimate;
    unsigned int passedPath;
    std::string path;
    unsigned int pathSize;

    unsigned int heuristic() const;
    bool tryUp();
    bool tryDown();
    bool tryRight();
    bool tryLeft();
    Field generateUp();
    Field generateDown();
    Field generateRight();
    Field generateLeft();

public:
    Field(std::vector< std::vector<short> > argTable);
    Field();
    Field(const Field& f);
    ~Field();
    Field& operator= (const Field& f);
    bool operator== (const Field& f) const;

    friend class Solver;
    friend class Comparator;
};

class Comparator {
public:
    bool operator()(const Field& f1, const Field& f2) const;
};

class Solver {
    Field start;

public:
    Solver(Field& f);
    ~Solver();
    std::string run();
};