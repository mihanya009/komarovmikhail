#include "Field.h"

int main() {
    Field f;
    Solver s(f);
    s.run();
    return 0;
}