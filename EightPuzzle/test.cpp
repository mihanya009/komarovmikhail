#include "Field.h"
#include <fstream>
#include <gtest/gtest.h>

TEST(CheckEasyCase, test_1) {
    std::vector< std::vector<short> > arg;
    std::vector<short> buf = {1, 2, 3};
    arg.push_back(buf);
    buf = {4, 5, 6};
    arg.push_back(buf);
    buf = {7, 0, 8};
    arg.push_back(buf);
    Field field(arg);
    Solver solver(field);

    EXPECT_EQ(solver.run(), "R");
}

TEST(CheckMiddleCase, test_2) {
    std::vector< std::vector<short> > arg;
    std::vector<short> buf = {0, 1, 6};
    arg.push_back(buf);
    buf = {4, 3, 2};
    arg.push_back(buf);
    buf = {7, 5, 8};
    arg.push_back(buf);
    Field field(arg);
    Solver solver(field);

    EXPECT_EQ(solver.run(), "RDRULDDR");
}

TEST(CheckHardCase, test_3) {
    std::vector< std::vector<short> > arg;
    std::vector<short> buf = {0, 1, 2};
    arg.push_back(buf);
    buf = {3, 4, 5};
    arg.push_back(buf);
    buf = {6, 7, 8};
    arg.push_back(buf);
    Field field(arg);
    Solver solver(field);

    EXPECT_EQ(solver.run(), "RDLDRRULLDRUURDDLLURRD");
}

TEST(CheckUnsolvable, test_4) {
    std::vector< std::vector<short> > arg;
    std::vector<short> buf = {1, 2, 3};
    arg.push_back(buf);
    buf = {8, 0, 4};
    arg.push_back(buf);
    buf = {7, 6, 5};
    arg.push_back(buf);
    Field field(arg);
    Solver solver(field);

    EXPECT_EQ(solver.run(), "-1");
}

int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

