#include <gtest/gtest.h>
#include "NumberOfSubStringsSeeker.h"

class UnitTest : public ::testing::Test {
protected:
    NumberOfSubStringsSeeker * seeker;

    void SetUp() {
        std::string arg = "abab";
        seeker = new NumberOfSubStringsSeeker(arg);
    }
    void TearDown() {
        delete seeker;
    }
};

TEST_F(UnitTest, testOperability_1) {
    ASSERT_EQ(seeker->run(), 7);
}

TEST_F(UnitTest, testOperability_2) {
    std::string arg = "helloworld";

    delete seeker;
    seeker = new NumberOfSubStringsSeeker(arg);

    ASSERT_EQ(seeker->run(), 52);
}

TEST_F(UnitTest, testOperability_3) {
    std::string arg = "abracadabra";

    delete seeker;
    seeker = new NumberOfSubStringsSeeker(arg);

    ASSERT_EQ(seeker->run(), 54);
}

int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

