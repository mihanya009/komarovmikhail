#ifndef TASK_3_D_NUMBEROFSUBSTRINGSSEEKER_H
#define TASK_3_D_NUMBEROFSUBSTRINGSSEEKER_H

#include <string>
#include <vector>

class NumberOfSubStringsSeeker {
    const unsigned charCount = 256;

    std::string str;
    unsigned int stringLength;

    std::vector<unsigned int> buildSuffixArray();
    std::vector<int> buildLCP();

public:
    NumberOfSubStringsSeeker(const std::string& s);

    int run();
};


#endif //TASK_3_D_NUMBEROFSUBSTRINGSSEEKER_H
