#include <iostream>
#include "NumberOfSubStringsSeeker.h"

int main() {
    std::string s;
    std::cin >> s;
    NumberOfSubStringsSeeker builder(s);
    std::cout << builder.run() << std::endl;
    return 0;
}