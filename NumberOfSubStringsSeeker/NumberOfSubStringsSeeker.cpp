#include "NumberOfSubStringsSeeker.h"

std::vector<unsigned int> NumberOfSubStringsSeeker::buildSuffixArray() {
    int sum = 0;
    std::vector<unsigned int> result(stringLength , 0);
    std::vector<unsigned int> colors(stringLength, 0);
    std::vector<unsigned int> bufVector(stringLength, 0);
    std::vector<unsigned int> buckets(std::max(charCount, stringLength) , 0);

    for (unsigned int i = 0; i < stringLength; i++) {
        buckets[str[i]]++;
    }

    for (unsigned int i = 0; i < charCount; i++) {
        sum += buckets[i];
        buckets[i] = sum - buckets[i];
    }

    for (unsigned int i = 0; i < stringLength; i++) {
        result[buckets[str[i]]++] = i;
    }

    colors[result[0]] = 0;
    for (unsigned int i = 1; i < stringLength; i++) {
        if (str[result[i]] != str[result[i - 1]]) {
            colors[result[i]] = colors[result[i - 1]] + 1;
        }
        else {
            colors[result[i]] = colors[result[i - 1]];
        }
    }

    int cn = colors[result[stringLength - 1]] + 1;
    for (unsigned int j = 1; j < stringLength; j *= 2) {
        for (unsigned int i = 0; i < stringLength; i++) {
            buckets[i] = 0;
        }
        for (unsigned int i = 0; i < stringLength; i++) {
            buckets[colors[i]]++;
        }
        sum = 0;
        for (unsigned int i = 0; i < cn; i++) {
            sum += buckets[i];
            buckets[i] = sum - buckets[i];
        }
        for (unsigned int i = 0; i < stringLength; i++) {
            int bufIndex = buckets[colors[(result[i] - j + stringLength) % stringLength]]++;
            bufVector[bufIndex] = (result[i] - j + stringLength) % stringLength;
        }
        result = bufVector;
        bufVector[result[0]] = 0;
        for (unsigned int i = 1; i < stringLength; i++) {
            bufVector[result[i]] = bufVector[result[i - 1]] + (colors[result[i]] != colors[result[i - 1]] ||
                                                               colors[(result[i] + j) % stringLength] !=
                                                               colors[(result[i - 1] + j) % stringLength]);
        }
        cn = bufVector[result[stringLength - 1]] + 1;
        colors = bufVector;
    }
    return result;
}

std::vector<int> NumberOfSubStringsSeeker::buildLCP() {
    std::vector<int> result(stringLength, 0);
    std::vector<unsigned int> suffixArray = this->buildSuffixArray();
    std::vector<unsigned int> rank(stringLength);
    for (unsigned int i = 0; i < stringLength; i++) {
        rank[suffixArray[i]] = i;
    }
    int k = 0;
    for (int i = 0; i < stringLength; i++) {
        if (k > 0) {
            k--;
        }
        if (rank[i] == stringLength - 1) {
            result[stringLength - 1] = -1;
            k = 0;
            continue;
        }
        int j = suffixArray[rank[i] + 1];
        while ((std::max(i + k, j + k) < stringLength) && (str[i + k] == str[j + k])) {
            k++;
        }
        result[rank[i]] = k;
    }
    result[stringLength - 1] = 0;
    return result;
}

NumberOfSubStringsSeeker::NumberOfSubStringsSeeker(const std::string& s) {
    str = s + char(0);
    stringLength = (unsigned int) str.size();
}

int NumberOfSubStringsSeeker::run() {
    int result = 0;
    std::vector<unsigned int> suffixArray = this->buildSuffixArray();
    std::vector<int> lcp = this->buildLCP();

    for (unsigned int i = 0; i < stringLength; i++) {
        result += stringLength - 1 - suffixArray[i];
    }
    for (unsigned int i = 0; i < stringLength - 1; i++) {
        result -= lcp[i];
    }
    return result;
}