#include "SubStringSeeker.h"

std::vector<unsigned int> SubStringSeeker::prefixFunction(const std::string& str) {
    unsigned int n = (unsigned int) str.size();
    std::vector<unsigned int> result(n);
    result[0] = 0;
    unsigned int previousValue;
    for (unsigned int i = 1; i < n; i++) {
        previousValue = result[i - 1];
        while ((previousValue > 0) && (str[previousValue] != str[i])) {
            previousValue = result[previousValue - 1];
        }
        if (str[previousValue] == str[i]) {
            previousValue++;
        }
        result[i] = previousValue;
    }
    return result;
}

SubStringSeeker::SubStringSeeker(const std::string& p, const std::string& s) : pattern(p), str(s) {}

std::vector<unsigned int> SubStringSeeker::run() {
    std::vector<unsigned int> patternPrefixes = prefixFunction(pattern + "$");
    std::vector<unsigned int> result;
    unsigned int k = 0;
    for(unsigned int i = 0; i < str.size(); i++) {
        while ((k > 0) && (pattern[k] != str[i])) {
            k = patternPrefixes[k - 1];
        }
        if (str[i] == pattern[k]) {
            k++;
        }
        if (k == pattern.size()) {
            result.push_back(i - (unsigned int) pattern.size() + 1);
        }

    }
    return result;
}