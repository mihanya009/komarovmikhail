#include <gtest/gtest.h>
#include "SubStringSeeker.h"

class UnitTest : public ::testing::Test {
protected:
    SubStringSeeker * seeker;

    void SetUp() {
        std::string pattern = "abca";
        std::string str = "acbacbbacbabcabbcbabcabcbacbbacbabcbacbabcbacbbacbabcbacbbabcababcbacbcbabcacbabcbacba";

        seeker = new SubStringSeeker(pattern, str);
    }
    void TearDown() {
        delete seeker;
    }
};

TEST_F(UnitTest, testOperability_1) {
    std::vector<unsigned int> answer = {10, 18, 58, 72};

    ASSERT_EQ(seeker->run(), answer);
}

TEST_F(UnitTest, testOperability_2) {
    std::string pattern = "aba";
    std::string str = "abacababa";

    delete seeker;
    seeker = new SubStringSeeker(pattern, str);

    std::vector<unsigned int> answer = {0, 4, 6};

    ASSERT_EQ(seeker->run(), answer);
}

TEST_F(UnitTest, testEmptyAnswer) {
    std::string pattern = "abaababababab";
    std::string str = "abacababa";

    delete seeker;
    seeker = new SubStringSeeker(pattern, str);

    std::vector<unsigned int> answer;

    ASSERT_EQ(seeker->run(), answer);
}


int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
