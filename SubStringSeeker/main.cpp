#include <iostream>
#include "SubStringSeeker.h"

int main() {
    std::string str;
    std::string pattern;
    std::cin >> pattern >> str;
    SubStringSeeker solver(pattern, str);
    std::vector<unsigned int> result = solver.run();
    for (unsigned int i = 0; i < result.size(); i++) {
        std::cout << result[i] << " ";
    }
    return 0;
}