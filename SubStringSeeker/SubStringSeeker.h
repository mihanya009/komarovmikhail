#ifndef TASK_3_1_SUBSTRINGSEEKER_H
#define TASK_3_1_SUBSTRINGSEEKER_H

#include <string>
#include <vector>

class SubStringSeeker {
    std::string pattern;
    std::string str;

    static std::vector<unsigned int> prefixFunction(const std::string& str);

public:
    SubStringSeeker(const std::string& p, const std::string& s);
    std::vector<unsigned int> run();
};


#endif //TASK_3_1_SUBSTRINGSEEKER_H
