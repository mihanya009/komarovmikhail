#include <cmath>
#include <queue>
#include <iostream>
#include <fstream>
#include "Field.h"

	int Field::heuristic() const {
		int result = 0;

		/* ����� ������������� ���������� */
		for (int k = 0; k <= 15; k++) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					if (table[i][j] == k) {
						if (k == 0) {
							result += 2 - i + 2 - j;
						}
						else result += abs(i - (k - 1) / 4) + abs(j - (k - 1) % 4);
					}
				}
			}
		}
		return result;
	}

	bool Field::tryUp() {
		for (int i = 0; i < 4; i++) {
			if (table[0][i] == 0) return false;
		}
		return true;
	}
	bool Field::tryDown() {
		for (int i = 0; i < 4; i++) {
			if (table[3][i] == 0) return false;
		}
		return true;
	}
	bool Field::tryRight() {
		for (int i = 0; i < 4; i++) {
			if (table[i][3] == 0) return false;
		}
		return true;
	}
	bool Field::tryLeft() {
		for (int i = 0; i < 4; i++) {
			if (table[i][0] == 0) return false;
		}
		return true;
	}
	Field Field::generateUp() {
		Field buf(*this);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (buf.table[i][j] == 0) {
					buf.table[i][j] = buf.table[i - 1][j];
					buf.table[i - 1][j] = 0;
					return buf;
				}
			}
		}
		return buf;
	}
	Field Field::generateDown() {
		Field buf(*this);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (buf.table[i][j] == 0) {
					buf.table[i][j] = buf.table[i + 1][j];
					buf.table[i + 1][j] = 0;
					return buf;
				}
			}
		}
		return buf;
	}
	Field Field::generateRight() {
		Field buf(*this);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (buf.table[i][j] == 0) {
					buf.table[i][j] = buf.table[i][j + 1];
					buf.table[i][j + 1] = 0;
					return buf;
				}
			}
		}
		return buf;
	}
	Field Field::generateLeft() {
		Field buf(*this);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (buf.table[i][j] == 0) {
					buf.table[i][j] = buf.table[i][j - 1];
					buf.table[i][j - 1] = 0;
					return buf;
				}
			}
		}
		return buf;
	}

	Field::Field(std::vector< std::vector<short> > argTable) : pathSize(0) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				table[i][j] = argTable[i][j];
			}
		}
	}

	Field::Field() : pathSize(0) {
		std::ifstream fin("puzzle.in");

		/* �������������� ���� */
		short bufInt = 0;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				fin >> bufInt;
				table[i][j] = bufInt;
			}
		}
		fin.close();
		path.resize(80);
	}

	Field::Field(const Field& f) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) table[i][j] = f.table[i][j];
		}
		path = f.path;
		passedPath = f.passedPath;
		pathSize = f.pathSize;
		totalEstimate = f.totalEstimate;
	}

	Field::~Field() {}

	Field& Field::operator= (const Field& f) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				table[i][j] = f.table[i][j];
			}
		}
		path = f.path;
		passedPath = f.passedPath;
		totalEstimate = f.totalEstimate;
		pathSize = f.pathSize;
		return *this;
	}

	bool Field::operator== (const Field& f) const {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (table[i][j] != f.table[i][j]) return false;
			}
		}
		return true;
	}

	bool Comparator::operator()(const Field& f1, const Field& f2) const {
		return ((f1.totalEstimate) > (f2.totalEstimate));
	}


	Solver::Solver(Field& f) : start(f) {}
	Solver::~Solver() {}

	void Solver::run() {
		std::vector<int> check;
		int lineWithZero;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (start.table[i][j] != 0) check.push_back(start.table[i][j]);
				else lineWithZero = i + 1;
			}
		}
		int inverse = 0;
		for (int i = 0; i < (int)check.size(); i++) {
			for (int j = i + 1; j < (int)check.size(); j++) {
				if (check[i] > check[j]) inverse++;
			}
		}
		if ((inverse + lineWithZero) % 2 == 1) {
			std::cout << -1 << std::endl;
			return;
		}

		std::vector< std::vector<short> > f;
		{
			int k = 1;
			for (int i = 0; i < 4; i++) {
				std::vector<short> buf;
				f.push_back(buf);
				for (int j = 0; j < 4; j++) {
					if (k != 16) f[i].push_back(k++);
					else f[i].push_back(0);
				}
			}
		}
		Field finish(f);

		std::priority_queue<Field, std::vector<Field>, Comparator> q;
		std::vector<Field> v; //��������� �������������

		start.passedPath = 0;
		start.totalEstimate = start.passedPath + start.heuristic();

		q.push(start);

		while (q.size() != 0) {
			Field current(q.top());
			if (current == finish) {
				std::cout << current.pathSize << std::endl;
				for (int i = 0; i < current.pathSize; i++) std::cout << current.path[i];
				std::cout << std::endl;
				break;
			}
			q.pop();
			v.push_back(current);
			for (int i = 0; i < 4; i++) { //��������� ������� c current �������
				if ((i == 0) && current.tryUp()) {
					Field buf(current.generateUp());
					int k = 0;
					for (unsigned int j = 0; j < v.size(); j++) {
						if (v[j] == buf) k++;
					}
					if (k > 0) continue;
					buf.path[buf.pathSize++] = 'U';
					buf.passedPath = current.passedPath + 1;
					buf.totalEstimate = buf.passedPath + buf.heuristic();
					q.push(buf);
				}
				if ((i == 1) && current.tryDown()) {
					Field buf(current.generateDown());
					int k = 0;
					for (unsigned int j = 0; j < v.size(); j++) {
						if (v[j] == buf) k++;
					}
					if (k > 0) continue;
					buf.path[buf.pathSize++] = 'D';
					buf.passedPath = current.passedPath + 1;
					buf.totalEstimate = buf.passedPath + buf.heuristic();
					q.push(buf);
				}
				if ((i == 2) && current.tryRight()) {
					Field buf(current.generateRight());
					int k = 0;
					for (unsigned int j = 0; j < v.size(); j++) {
						if (v[j] == buf) k++;
					}
					if (k > 0) continue;
					buf.path[buf.pathSize++] = 'R';
					buf.passedPath = current.passedPath + 1;
					buf.totalEstimate = buf.passedPath + buf.heuristic();
					q.push(buf);
				}
				if ((i == 3) && current.tryLeft()) {
					Field buf(current.generateLeft());
					int k = 0;
					for (unsigned int j = 0; j < v.size(); j++) {
						if (v[j] == buf) k++;
					}
					if (k > 0) continue;
					buf.path[buf.pathSize++] = 'L';
					buf.passedPath = current.passedPath + 1;
					buf.totalEstimate = buf.passedPath + buf.heuristic();
					q.push(buf);
				}
			}
		}
	}
