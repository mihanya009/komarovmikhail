#include <gtest/gtest.h>
#include "SegmentTree.h"

class UnitTest : public ::testing::Test {
protected:
    SegmentTree * segmentTree;

    void SetUp() {
        std::vector<int> arg = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        segmentTree = new SegmentTree(arg);
    }
    void TearDown() {
        delete segmentTree;
    }
};

TEST_F(UnitTest, testOperability_1) {
    ASSERT_EQ(segmentTree->findSecondStatistic(1, 2), 2);
    ASSERT_EQ(segmentTree->findSecondStatistic(1, 10), 2);
    ASSERT_EQ(segmentTree->findSecondStatistic(2, 7), 3);
}

TEST_F(UnitTest, testOperability_2) {
    std::vector<int> arg = {8, 1, 2, 5, 6, 4, 3};

    delete segmentTree;
    segmentTree = new SegmentTree(arg);

    ASSERT_EQ(segmentTree->findSecondStatistic(2, 6), 2);
    ASSERT_EQ(segmentTree->findSecondStatistic(3, 6), 4);
    ASSERT_EQ(segmentTree->findSecondStatistic(3, 7), 3);
}

TEST_F(UnitTest, testOperability_3) {
    std::vector<int> arg = {5, 6, 8, 1, 3, 2, 9, 6, 7, 5, 5, 4};

    delete segmentTree;
    segmentTree = new SegmentTree(arg);

    ASSERT_EQ(segmentTree->findSecondStatistic(2, 9), 2);
    ASSERT_EQ(segmentTree->findSecondStatistic(1, 12), 2);
    ASSERT_EQ(segmentTree->findSecondStatistic(10, 12), 5);
}

int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
