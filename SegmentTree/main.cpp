#include <iostream>
#include "SegmentTree.h"

int main() {
    int arrSize;
    int segmentCount;
    std::cin >> arrSize >> segmentCount;
    std::vector<int> arr;
    arr.resize((unsigned long) arrSize);
    for (int i = 0; i < arrSize; i++) {
        std::cin >> arr[i];
    }
    SegmentTree st(arr);

    int begin, end;
    std::vector<int> result;
    for (int i = 0; i < segmentCount; i++) {
        std::cin >> begin >> end;
        result.push_back(st.findSecondStatistic(begin, end));
    }
    for (unsigned int i = 0; i < result.size(); i++) {
        std::cout << result[i] << std::endl;
    }
    return 0;
}