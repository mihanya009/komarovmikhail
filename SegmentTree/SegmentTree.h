#ifndef SECONDSTATISTIC_SEGMENTTREE_H
#define SECONDSTATISTIC_SEGMENTTREE_H

#include <vector>

class SegmentTree;

class Pair {
    int first;
    int second;

    Pair(int a, int b);
    Pair();

    friend class SegmentTree;
};

class SegmentTree {
    std::vector<Pair> table;
    std::vector<int> array;
    unsigned int arraySize;
    unsigned int tableSize;

    static Pair mergeCurrentResults(Pair p1, Pair p2);
    void preProcessing();

public:
    SegmentTree(std::vector<int> arr);
    int findSecondStatistic(int left, int right);
};

#endif //SECONDSTATISTIC_SEGMENTTREE_H
