#include "SegmentTree.h"
#include <climits>

Pair::Pair(int a, int b) : first(a), second(b) {}

Pair::Pair() {}

Pair SegmentTree::mergeCurrentResults(Pair p1, Pair p2) {
    Pair result;
    result.first = std::min(p1.first, p2.first);
    if (p1.second == p2.second) {
        result.second = std::max(p1.first, p2.first);
    }
    else {
        int bufInt = std::min(p1.second, p2.second);
        if ((bufInt > p1.first) && (bufInt > p2.first)) {
            result.second = std::max(p1.first, p2.first);
        }
        else {
            result.second = bufInt;
        }
    }
    return result;
}

void SegmentTree::preProcessing() {
    unsigned int degree = 0;
    unsigned int k = 1;
    while (k < arraySize) {
        k *= 2;
        degree++;
    }
    tableSize = 2 * k - 1;
    const Pair bufPair(INT_MAX, INT_MAX);
    table.resize(tableSize, bufPair);

    for (unsigned int i = k - 1; i < k - 1 + arraySize; i++) {
        table[i].first = array[i - k + 1];
    }

    for (int i = tableSize / 2 - 1; i >= 0; i--) {
        table[i] = mergeCurrentResults(table[2 * i + 1], table[2 * i + 2]);
    }
}

SegmentTree::SegmentTree(std::vector<int> arr) {
    array = arr;
    arraySize = (unsigned int) arr.size();

    preProcessing();
}

int SegmentTree::findSecondStatistic(int left, int right) {
    Pair result(INT_MAX, INT_MAX);
    left += (tableSize / 2) - 1;
    right += (tableSize / 2) - 1;

    while (left <= right) {
        if ((left & 1) == 0) {
            result = mergeCurrentResults(result, table[left]);
        }
        if ((right & 1) == 1) {
            result = mergeCurrentResults(result, table[right]);
        }
        if ((right - 2 < 0) || (left < 0)) {
            break;
        }
        left = left / 2;
        right = (right - 2) / 2;
    }
    return result.second;
}