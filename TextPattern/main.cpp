#include <iostream>
#include "PatternSeeker.h"

int main() {
    std::string str, pattern;
    std::cin >> pattern >> str;

    PatternSeeker p(pattern, str);
    std::vector<int> result = p.run();

    for (unsigned int i = 0; i < result.size(); i++) {
        std::cout << result[i] << "\n";
    }
    return 0;
}