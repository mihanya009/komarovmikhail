#include "PatternSeeker.h"

Node::Node() {
    for (unsigned int i = 0; i < 26; i++) {
        next[i] = -1;
        go[i] = -1;
    }
    leaf = false;
    link = -1;
    parent = -1;
    up = -1;
}

Automate::Automate() : size(1) {
    Node bufNode;
    nodes.push_back(bufNode);
}

bool Automate::isLeaf(int nodeArg) {
    return nodes[nodeArg].leaf;
}

void Automate::addString (std::string str, int start, int suffSize) {
    int currentNode = 0;
    for (unsigned int i = 0; i < str.size(); i++) {
        char charIndex = str[i] - 'a';
        if (nodes[currentNode].next[charIndex] == -1) {
            Node bufNode;
            nodes.push_back(bufNode);
            nodes[size].link = -1;
            nodes[size].parent = currentNode;
            nodes[size].parentChar = charIndex;
            nodes[currentNode].next[charIndex] = size++;
        }
        currentNode = nodes[currentNode].next[charIndex];
    }
    nodes[currentNode].leaf = true;
    nodes[currentNode].startPos.push_back(start);
    nodes[currentNode].suffixSize.push_back(suffSize);
}

int Automate::getLink(int nodeArg) {
    if (nodes[nodeArg].link == -1) {
        if ((nodeArg == 0) || (nodes[nodeArg].parent == 0)) {
            nodes[nodeArg].link = 0;
        }
        else {
            nodes[nodeArg].link = go(getLink(nodes[nodeArg].parent), nodes[nodeArg].parentChar);
        }
    }
    return nodes[nodeArg].link;
}

int Automate::go(int nodeArg, char charArg) {
    if (nodes[nodeArg].go[charArg] == -1) {
        if (nodes[nodeArg].next[charArg] != -1) {
            nodes[nodeArg].go[charArg] = nodes[nodeArg].next[charArg];
        }
        else {
            nodes[nodeArg].go[charArg] = (nodeArg == 0 ? 0 : go(getLink(nodeArg), charArg));
        }
    }
    return nodes[nodeArg].go[charArg];
}

int Automate::getUp(int nodeArg) {
    if (nodes[nodeArg].up == -1) {
        if (nodes[getLink(nodeArg)].leaf) {
            nodes[nodeArg].up = getLink(nodeArg);
        }
        else if (getLink(nodeArg) == 0) {
            nodes[nodeArg].up = 0;
        }
        else {
            nodes[nodeArg].up = getUp(getLink(nodeArg));
        }
    }
    return nodes[nodeArg].up;
}

void PatternSeeker::buildAutomate() {
    unsigned int start = 0;
    for (unsigned int i = 0; i < pattern.size(); i++) {
        if (i != 0) {
            if ((pattern[i] == '?') && (pattern[i - 1] != '?')) {
                suffixAutomate.addString(pattern.substr(start, i - start), start, i - start);
                subStringsCount++;
            }
            if ((pattern[i] != '?') && (pattern[i - 1] == '?')) {
                start = i;
            }
        }
        else {
            if (pattern[i] == '?') {
                start = 1;
            }
        }
    }
    if (pattern[pattern.size() - 1] != '?') {
        suffixAutomate.addString(pattern.substr(start, pattern.size()), start, pattern.size() - start);
        subStringsCount++;
    }
}

PatternSeeker::PatternSeeker(std::string p, std::string t) : pattern(p), text(t) {
    subStringsCount = 0;
    buildAutomate();
}

std::vector<int> PatternSeeker::run() {
    std::vector<int> result;
    if (pattern.size() > text.size()) {
        return result;
    }

    std::vector<int> pos(text.size(), 0);
    int current = 0;
    for (unsigned int i = 0; i < text.size(); i++) {
        char c = text[i] - 'a';
        current = suffixAutomate.go(current, c);
        if (suffixAutomate.isLeaf(current)) {
            for (unsigned int j = 0; j < suffixAutomate.nodes[current].suffixSize.size(); j++) {
                int index = (int) i - suffixAutomate.nodes[current].suffixSize[j] -
                            suffixAutomate.nodes[current].startPos[j] + 1;
                if (index >= 0) {
                    pos[index]++;
                }
            }
        }
        int upLink = suffixAutomate.getUp(current);
        while (upLink != 0) {
            for (unsigned int j = 0; j < suffixAutomate.nodes[upLink].suffixSize.size(); j++) {
                int index = (int) i - suffixAutomate.nodes[upLink].suffixSize[j] -
                            suffixAutomate.nodes[upLink].startPos[j] + 1;
                if (index >= 0) {
                    pos[index]++;
                }
            }
            upLink = suffixAutomate.getUp(upLink);
        }
    }

    for (unsigned int i = 0; i < pos.size(); i++) {
        if ((pos[i] == subStringsCount) && (pos.size() - i >= pattern.size())) {
            result.push_back(i);
        }
    }

    return result;
}