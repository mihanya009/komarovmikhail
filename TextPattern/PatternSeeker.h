#ifndef TASK_3_C_PATTERNSEEKER_H
#define TASK_3_C_PATTERNSEEKER_H

#include <vector>
#include <string>

class Automate;
class PatternSeeker;

class Node {
    int next[26];
    int go[26];
    bool leaf;
    int parent;
    char parentChar;
    int link;
    int up;
    std::vector<int> startPos;
    std::vector<int> suffixSize;

    Node();

    friend class Automate;
    friend class PatternSeeker;
};

class Automate {
    std::vector<Node> nodes;
    int size;

public:
    Automate();

    bool isLeaf(int nodeArg);
    void addString (std::string str, int start, int suffSize);
    int getLink(int nodeArg);
    int go(int nodeArg, char charArg);
    int getUp(int nodeArg);

    friend class PatternSeeker;
};

class PatternSeeker {
    std::string pattern;
    std::string text;
    Automate suffixAutomate;
    unsigned int subStringsCount;

    void buildAutomate();

public:
    PatternSeeker(std::string p, std::string t);

    std::vector<int> run();
};

#endif //TASK_3_C_PATTERNSEEKER_H
