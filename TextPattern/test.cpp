#include <gtest/gtest.h>
#include "PatternSeeker.h"

class UnitTest : public ::testing::Test {
protected:
    PatternSeeker * patternSeeker;

    void SetUp() {
        std::string pattern = "ab?b";
        std::string text = "ababcabdb";
        patternSeeker = new PatternSeeker(pattern, text);
    }
    void TearDown() {
        delete patternSeeker;
    }
};

TEST_F(UnitTest, testOperability_1) {
    std::vector<int> answer = {0, 5};
    ASSERT_EQ(patternSeeker->run(), answer);
}

TEST_F(UnitTest, testOperability_2) {
    std::string pattern = "ab?b";
    std::string text = "abababab";

    delete patternSeeker;
    patternSeeker = new PatternSeeker(pattern, text);

    std::vector<int> answer = {0, 2, 4};

    ASSERT_EQ(patternSeeker->run(), answer);
}

TEST_F(UnitTest, testDuplicatedSubStrings) {
    std::string pattern = "a?a?";
    std::string text = "aaaaaaa";

    delete patternSeeker;
    patternSeeker = new PatternSeeker(pattern, text);

    std::vector<int> answer = {0, 1, 2, 3};

    ASSERT_EQ(patternSeeker->run(), answer);
}

TEST_F(UnitTest, testEmptyAnswer) {
    std::string pattern = "ab?ab??";
    std::string text = "cccabcabc";

    delete patternSeeker;
    patternSeeker = new PatternSeeker(pattern, text);

    std::vector<int> answer;

    ASSERT_EQ(patternSeeker->run(), answer);

    pattern = "ab?ab";
    text = "abab";

    delete patternSeeker;
    patternSeeker = new PatternSeeker(pattern, text);

    ASSERT_EQ(patternSeeker->run(), answer);
}

int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

