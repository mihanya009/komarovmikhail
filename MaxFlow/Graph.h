//
// Created by mikhail on 14.11.16.
//

#ifndef MAXFLOW2_GRAPH_H
#define MAXFLOW2_GRAPH_H

#include <vector>
#include <fstream>

class Graph {
    unsigned int nodeCount;
    unsigned int edgeCount;
    unsigned int source;
    unsigned int drain;
    std::vector< std::vector<int> > table;

    void tableInit();

public:
    Graph(unsigned int n, std::ifstream& fin);
    ~Graph();

    friend class MaxFlowSeeker;
};


#endif //MAXFLOW2_GRAPH_H
