//
// Created by mikhail on 14.11.16.
//

#ifndef MAXFLOW2_MAXFLOWSEEKER_H
#define MAXFLOW2_MAXFLOWSEEKER_H

#include "Graph.h"
#include <climits>
#define INF INT_MAX;

class MaxFlowSeeker {
    Graph graph;
    std::vector<int> excess;
    std::vector<int> height;
    std::vector< std::vector<int> > preFlow;

    void initPreFlow();

    static int min(int a, int b);

    static int max(int a, int b);

    void push(unsigned int i, unsigned int j);

    void relable(unsigned int u);

public:
    MaxFlowSeeker(Graph& g);

    int run();
};


#endif //MAXFLOW2_MAXFLOWSEEKER_H
