#include "Graph.h"
#include "MaxFlowSeeker.h"

int main() {
    unsigned int nodeCount = 1;
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");
    while (nodeCount != 0) {
        unsigned int buf;
        fin >> buf;
        if (buf == 0) {
            break;
        }
        nodeCount = buf;
        Graph g(nodeCount, fin);
        MaxFlowSeeker solver(g);
        fout << solver.run() << std::endl;
    }
    fin.close();
    fout.close();
    return 0;
}