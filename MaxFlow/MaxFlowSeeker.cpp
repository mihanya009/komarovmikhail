//
// Created by mikhail on 14.11.16.
//

#include "MaxFlowSeeker.h"

void MaxFlowSeeker::initPreFlow() {
    unsigned int  n = graph.nodeCount;
    for (unsigned int i = 0; i < n; i++) {
        height.push_back(0);
        excess.push_back(0);
    }
    for (unsigned int i = 0; i < n; i++) {
        std::vector<int> bufVector;
        preFlow.push_back(bufVector);
        for (unsigned int j = 0; j < n; j++) {
            preFlow[i].push_back(0);
        }
    }
    for (unsigned int i = 0; i < n; i++) {
        int currentCapacity = graph.table[graph.source - 1][i];
        if (currentCapacity != 0) {
            preFlow[graph.source - 1][i] = currentCapacity;
            preFlow[i][graph.source - 1] = -currentCapacity;
            excess[i] = currentCapacity;
            excess[graph.source - 1] -= currentCapacity;
        }
    }
    height[graph.source - 1] = n;
}

int MaxFlowSeeker::min(int a, int b) {
    if (a < b) {
        return a;
    }
    else {
        return b;
    }
}

int MaxFlowSeeker::max(int a, int b) {
    if (a > b) {
        return a;
    }
    else {
        return b;
    }
}

void MaxFlowSeeker::push(unsigned int i, unsigned int j) {
    int d = min(excess[i], graph.table[i][j] - preFlow[i][j]);
    preFlow[i][j] += d;
    preFlow[j][i] = -preFlow[i][j];
    excess[i] -= d;
    excess[j] += d;
}

void MaxFlowSeeker::relable(unsigned int u) {
    int minHeight = INF;
    for (unsigned int i = 0; i < graph.nodeCount; i++) {
        if ((graph.table[u][i] != 0) && (graph.table[u][i] - preFlow[u][i] > 0)) {
            minHeight = min(height[i], minHeight);
        }
    }
    if (minHeight == INT_MAX) {
        return;
    }
    height[u] = minHeight + 1;
}


MaxFlowSeeker::MaxFlowSeeker(Graph& g) : graph(g) {
        initPreFlow();
}

int MaxFlowSeeker::run() {
    unsigned int i;
    unsigned int j;
    while (true) {
        for (i = 0; i < graph.nodeCount; i++) {
            if ((excess[i] > 0) && (i != graph.source - 1) && (i != graph.drain - 1)) {
                break;
            }
        }
        if (i == graph.nodeCount) {
            break;
        }
        for (j = 0; j < graph.nodeCount; j++) {
            if ((graph.table[i][j] - preFlow[i][j] > 0) && (height[i] == height[j] + 1)) {
                break;
            }
        }
        if (j < graph.nodeCount) {
            push(i, j);
        }
        else {
            relable(i);
        }
    }
    int flow = 0;
    for (unsigned int i = 0; i < graph.nodeCount; i++) {
        if (graph.table[i][graph.drain - 1] != 0) {
            flow += preFlow[i][graph.drain - 1];
        }
    }
    return max(flow, 0);
}
