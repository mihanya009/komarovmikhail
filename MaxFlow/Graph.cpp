#include "Graph.h"//
// Created by mikhail on 14.11.16.
//

void Graph::tableInit() {
    for (int i = 0; i < nodeCount; i++) {
        std::vector<int> bufVector;
        table.push_back(bufVector);
        for (int j = 0; j < nodeCount; j++) {
            table[i].push_back(0);
        }
    }
}

Graph::Graph(unsigned int n, std::ifstream& fin) : nodeCount(n) {
    fin >> source >> drain >> edgeCount;
    tableInit();
    int first, second, capacity;
    for (unsigned int i = 0; i < edgeCount; i++) {
        fin >> first >> second >> capacity;
        table[first - 1][second - 1] += capacity;
        table[second - 1][first - 1] += capacity;
    }
}

Graph::~Graph() {}

