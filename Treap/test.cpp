#include "Treap.h"
#include "BinarySearchTree.h"
#include <gtest/gtest.h>

TEST(CheckTreap, test_1) {
    Treap treap;
    treap.insert(1, 2);
    treap.insert(4, 5);
    treap.insert(94, 25);
    treap.insert(10, 23);
    treap.insert(36, 42);
    treap.insert(51, 9);
    treap.insert(23, 80);
    treap.insert(40, 63);
    treap.insert(24, 12);
    treap.insert(16, 3);
    treap.insert(78, 87);
    treap.insert(59, 69);
    treap.insert(17, 18);

    ASSERT_EQ(treap.getHeight(), 6);
    ASSERT_EQ(treap.getWidth(), 4);

    ASSERT_EQ(treap.getSize(), 13);

    treap.remove(16);
    ASSERT_EQ(treap.getSize(), 12);

    treap.remove(94);
    ASSERT_EQ(treap.getSize(), 11);

    treap.remove(55);
    ASSERT_EQ(treap.getSize(), 11);

    ASSERT_EQ(treap.getHeight(), 6);
    ASSERT_EQ(treap.getWidth(), 3);
}

TEST(CheckBinarySearchTree, test_2) {
    BinarySearchTree bst;

    bst.insert(1);
    bst.insert(4);
    bst.insert(94);
    bst.insert(10);
    bst.insert(36);
    bst.insert(51);
    bst.insert(23);
    bst.insert(40);
    bst.insert(24);
    bst.insert(16);
    bst.insert(78);
    bst.insert(59);
    bst.insert(17);

    ASSERT_EQ(bst.getHeight(), 8);
    ASSERT_EQ(bst.getWidth(), 4);

    ASSERT_EQ(bst.getSize(), 13);

    bst.remove(16);
    ASSERT_EQ(bst.getSize(), 12);

    bst.remove(94);
    ASSERT_EQ(bst.getSize(), 11);

    bst.remove(55);
    ASSERT_EQ(bst.getSize(), 11);

    ASSERT_EQ(bst.getHeight(), 7);
    ASSERT_EQ(bst.getWidth(), 4);
}


int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
