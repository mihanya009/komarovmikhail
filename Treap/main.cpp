#include <iostream>
#include "Treap.h"
#include "BinarySearchTree.h"

int main() {
    Treap t;
    BinarySearchTree bst;
    int n;
    std::cin >> n;
    for (int i = 0; i < n; i++) {
        int a, b;
        std::cin >> a >> b;
        t.insert(a, b);
        bst.insert(a);
    }
    std::cout << t.getWidth() - bst.getWidth() << std::endl;

    return 0;
}