cmake_minimum_required(VERSION 3.6)
project(Treap)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

enable_testing()

set(TEST_FILES test.cpp Treap.cpp Treap.h BinarySearchTree.cpp BinarySearchTree.h)
add_executable(TreapTest ${TEST_FILES})
target_link_libraries(TreapTest gtest gtest_main)

set(SOURCE_FILES main.cpp Treap.cpp Treap.h BinarySearchTree.cpp BinarySearchTree.h)
add_executable(Treap ${SOURCE_FILES})