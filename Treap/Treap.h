#ifndef TREAP_TREAP_H
#define TREAP_TREAP_H

#include <memory>
#include <vector>
#include <stack>

class Treap;

class Node {
    int x, y;
    std::shared_ptr<Node> left;
    std::shared_ptr<Node> right;

    Node(int a, int b);
    Node();

    friend class Treap;
};

class Treap {
    std::shared_ptr<Node> root;
    unsigned int size;
    std::stack<int> nodesLevels;

    static void split(std::shared_ptr<Node> currentNode, int key, std::shared_ptr<Node>& leftNode,
                      std::shared_ptr<Node>& rightNode);
    static std::shared_ptr<Node> merge(std::shared_ptr<Node> first, std::shared_ptr<Node> second);
    static int getHeight(std::shared_ptr<Node> node, int h);
    void getWidth(std::shared_ptr<Node> node, int w);

public:
    Treap();

    void insert(int x, int y);
    void remove(int x);
    int getSize();
    int getHeight();
    int getWidth();
};


#endif //TREAP_TREAP_H
