#ifndef TREAP_BINARYSEARCHTREE_H
#define TREAP_BINARYSEARCHTREE_H

#include <memory>
#include <vector>
#include <stack>

class BinarySearchTree;

class SimpleNode {
    int x;
    std::shared_ptr<SimpleNode> left;
    std::shared_ptr<SimpleNode> right;

    SimpleNode(int a);
    SimpleNode();

    friend class BinarySearchTree;
};

class BinarySearchTree {
    std::shared_ptr<SimpleNode> root;
    unsigned int size;
    std::stack<int> nodesLevels;

    static int getHeight(std::shared_ptr<SimpleNode> node, int h);
    void getWidth(std::shared_ptr<SimpleNode> node, int h);

public:
    BinarySearchTree();
    void insert(int key);
    void remove(int key);
    int getSize();
    int getHeight();
    int getWidth();
};


#endif //TREAP_BINARYSEARCHTREE_H
