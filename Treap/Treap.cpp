#include "Treap.h"

Treap::Treap() {
    root = NULL;
    size = 0;
}

void Treap::split(std::shared_ptr<Node> currentNode, int key, std::shared_ptr<Node> &leftNode,
                  std::shared_ptr<Node>& rightNode) {
    if (currentNode == NULL) {
        leftNode = NULL;
        rightNode = NULL;
    }
    else {
        if (key >= currentNode->x) {
            split(currentNode->right, key, currentNode->right, rightNode);
            leftNode = currentNode;
        }
        else {
            split(currentNode->left, key, leftNode, currentNode->left);
            rightNode = currentNode;
        }
    }
}

std::shared_ptr<Node> Treap::merge(std::shared_ptr<Node> first, std::shared_ptr<Node> second) {
    if (first == NULL) {
        return second;
    }
    if (second == NULL) {
        return first;
    }
    if (first->y > second->y) {
        first->right = merge(first->right, second);
        return first;
    }
    else {
        second->left = merge(first, second->left);
        return second;
    }
}

void Treap::insert(int x, int y) {
    bool previousTurn; //true - right, false - left;
    std::shared_ptr<Node> parent = NULL;
    std::shared_ptr<Node> currentNode = root;
    if (currentNode != NULL) {
        while ((currentNode->y >= y) && (currentNode != NULL)) {
            if (currentNode->x >= x) {
                parent = currentNode;
                previousTurn = false;
                currentNode = currentNode->left;
            } else {
                parent = currentNode;
                previousTurn = true;
                currentNode = currentNode->right;
            }
            if (currentNode == NULL) {
                break;
            }
        }
    }
    std::shared_ptr<Node> newLeft;
    std::shared_ptr<Node> newRight;
    split(currentNode, x, newLeft, newRight);
    Node bufNode(x, y);
    bufNode.left = newLeft;
    bufNode.right = newRight;
    std::shared_ptr<Node> newNode = std::make_shared<Node>(bufNode);
    if (parent != NULL) {
        if (previousTurn) {
            parent->right = newNode;
        } else {
            parent->left = newNode;
        }
    }
    else {
        root = newNode;
    }
    size++;
}

void Treap::remove(int x) {
    bool previousTurn; //true - right, false - left;
    std::shared_ptr<Node> parent = NULL;
    std::shared_ptr<Node> currentNode = root;
    while (currentNode != NULL) {
        if (currentNode->x > x) {
            previousTurn = false;
            parent = currentNode;
            currentNode = currentNode->left;
            continue;
        }
        if (currentNode->x < x) {
            previousTurn = true;
            parent = currentNode;
            currentNode = currentNode->right;
            continue;
        }
        if (currentNode->x == x) {
            break;
        }
    }
    if (currentNode != NULL) {
        std::shared_ptr<Node> newNode = merge(currentNode->left, currentNode->right);
        if (parent == NULL) {
            root = newNode;
            size--;
            return;
        }
        if (previousTurn) {
            parent->right = newNode;
        }
        else {
            parent->left = newNode;
        }
        size--;
    }
}

int Treap::getSize() {
    return size;
}

int Treap::getHeight(std::shared_ptr<Node> node, int h) {
    if (node == NULL) {
        return h;
    }
    else {
        return std::max(getHeight(node->right, h + 1), getHeight(node->left, h + 1));
    }
}

int Treap::getHeight() {
    return getHeight(root, 0);
}

void Treap::getWidth(std::shared_ptr<Node> node, int h) {
    if (node != NULL) {
        nodesLevels.push(h);
        getWidth(node->left, h + 1);
        getWidth(node->right, h + 1);
    }
    else {
        return;
    }
}

int Treap::getWidth() {
    int height = getHeight();
    std::vector<int> levels(height, 0);
    getWidth(root, 0);
    while (!nodesLevels.empty()) {
        levels[nodesLevels.top()]++;
        nodesLevels.pop();
    }
    int max = 0;
    for (unsigned int i = 0; i < levels.size(); i++) {
        max = std::max(max, levels[i]);
    }
    return max;
}

Node::Node(int a, int b) : x(a), y(b) {
    left = NULL;
    right = NULL;
}

Node::Node() {}