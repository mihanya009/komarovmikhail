#include "BinarySearchTree.h"

void BinarySearchTree::insert(int key) {
    if (size == 0) {
        SimpleNode bufNode(key);
        root = std::make_shared<SimpleNode>(bufNode);
        size++;
        return;
    }

    bool previousTurn;
    std::shared_ptr<SimpleNode> parent = NULL;
    std::shared_ptr<SimpleNode> currentNode = root;
    while (currentNode != NULL) {
        if (key < currentNode->x) {
            previousTurn = false;
            parent = currentNode;
            currentNode = currentNode->left;
        }
        else {
            previousTurn = true;
            parent = currentNode;
            currentNode = currentNode->right;
        }
    }
    if (previousTurn) {
        SimpleNode bufNode(key);
        parent->right = std::make_shared<SimpleNode>(bufNode);
    }
    else {
        SimpleNode bufNode(key);
        parent->left = std::make_shared<SimpleNode>(bufNode);
    }
    size++;
}

BinarySearchTree::BinarySearchTree() {
    root = NULL;
    size = 0;
}

void BinarySearchTree::remove(int key) {
    if (size == 0) {
        return;;
    }

    bool previousTurn;
    std::shared_ptr<SimpleNode> parent = NULL;
    std::shared_ptr<SimpleNode> currentNode = root;
    while (currentNode != NULL) {
        if (key < currentNode->x) {
            previousTurn = false;
            parent = currentNode;
            currentNode = currentNode->left;
            continue;
        }
        if (key > currentNode->x) {
            previousTurn = true;
            parent = currentNode;
            currentNode = currentNode->right;
            continue;
        }
        if (key == currentNode->x) {
            if ((currentNode->left == NULL) && (currentNode->right == NULL)) {
                if (previousTurn) {
                    parent->right = NULL;
                }
                else {
                    parent->left = NULL;
                }
            }
            if ((currentNode->left == NULL) ^ (currentNode->right == NULL)) {
                bool index;//1 - right child is not NULL, 0 - the left one
                if (currentNode->right == NULL) {
                    index = 0;
                }
                else {
                    index = 1;
                }
                if (previousTurn) {
                    if (index) {
                        parent->right = currentNode->right;
                    }
                    else {
                        parent->right = currentNode->left;
                    }
                }
                else {
                    if (index) {
                        parent->left = currentNode->right;
                    }
                    else {
                        parent->left = currentNode->left;
                    }
                }
            }
            if ((currentNode->left != NULL) && (currentNode->right != NULL)) {
                std::shared_ptr<SimpleNode> buf; //node with key = min{key > current->key};
                std::shared_ptr<SimpleNode> bufParent; //buf's parent
                buf = currentNode->right;
                bufParent = currentNode;
                while (buf->left != NULL) {
                    bufParent = buf;
                    buf = buf->left;
                }
                bufParent->left = buf->right;
                buf->left = currentNode->left;
                if (previousTurn) {
                    parent->right = buf;
                }
                else {
                    parent->left = buf;
                }
            }
            size--;
            break;
        }
    }
}

int BinarySearchTree::getSize() {
    return size;
}

int BinarySearchTree::getHeight(std::shared_ptr<SimpleNode> node, int h) {
    if (node == NULL) {
        return h;
    }
    else {
        return std::max(getHeight(node->right, h + 1), getHeight(node->left, h + 1));
    }
}

int BinarySearchTree::getHeight() {
    return getHeight(root, 0);
}

void BinarySearchTree::getWidth(std::shared_ptr<SimpleNode> node, int h) {
    if (node != NULL) {
        nodesLevels.push(h);
        getWidth(node->left, h + 1);
        getWidth(node->right, h + 1);
    }
    else {
        return;
    }
}

int BinarySearchTree::getWidth() {
    int height = getHeight();
    std::vector<int> levels(height, 0);
    getWidth(root, 0);
    while (!nodesLevels.empty()) {
        levels[nodesLevels.top()]++;
        nodesLevels.pop();
    }
    int max = 0;
    for (unsigned int i = 0; i < levels.size(); i++) {
        max = std::max(max, levels[i]);
    }
    return max;
}

SimpleNode::SimpleNode(int a) : x(a) {
    left = NULL;
    right = NULL;
}

SimpleNode::SimpleNode() {}