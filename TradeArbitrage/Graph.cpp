#include <vector>
#include "Graph.h"

float min(float a, float b) { //if a < 0 => a = infinity
    if (a < 0) return b;
    if (b < 0) return a;
    return (a < b) ? a : b;
}

Graph::Graph() : nodeCount(0) {
    initFromStream(std::cin);
}

Graph::Graph(const Graph& g) {
    nodeCount = g.nodeCount;
    table = g.table;
}

Graph::Graph(std::vector<std::vector<float > > arg) {
    table = arg;
    nodeCount = (int) arg.size();
}

void Graph::initFromStream(std::istream& in) {
    in >> nodeCount;

    float bufInt = 0;
    for (int i = 0; i < nodeCount; i++) {
        std::vector<float> bufVector;
        table.push_back(bufVector);
        for (int j = 0; j < nodeCount; j++) {
            if (i != j) {
                in >> bufInt;
                table[i].push_back(1 / bufInt);
            }
            else table[i].push_back(1);
        }
    }
}

int Graph::getNodeCount() {
    return nodeCount;
}

Graph& Graph::operator= (const Graph& g) {
    nodeCount = g.nodeCount;
    table = g.table;
    return *this;
}

TradeArbitrage::TradeArbitrage(Graph& object) : g(&object) {}

std::string TradeArbitrage::findArbitrage() {
    unsigned int circleCounter = 0;
    std::vector< std::vector<float> > shortestDistanceTable;
    shortestDistanceTable = g->table;
    int n = g->nodeCount;
    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((shortestDistanceTable[i][k] > 0) || (shortestDistanceTable[k][j] > 0)) {
                    shortestDistanceTable[i][j] = min(shortestDistanceTable[i][j],
                                                      shortestDistanceTable[i][k] * shortestDistanceTable[k][j]);
                }
                if ((g->table[j][i] != -1) && (shortestDistanceTable[i][j] != -1)) {
                    if (shortestDistanceTable[i][j] * g->table[j][i] < 1) {
                        circleCounter++;
                        break;
                    }
                }
            }
            if (circleCounter > 0) break;
        }
        if (circleCounter > 0) break;
    }
    if (circleCounter > 0) {
        std::cout << "YES\n";
        return "YES";
    }
    else {
        std::cout << "NO\n";
        return "NO";
    }
}