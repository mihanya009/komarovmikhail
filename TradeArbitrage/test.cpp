#include <gtest/gtest.h>
#include "Graph.h"

class UnitTest : public ::testing::Test {
protected:
    Graph * graph;
    TradeArbitrage * solutor;
    void SetUp() {
        std::vector< std::vector<float> > argVec;
        std::vector<float> bufVec = {1, (1 / 10.0)};
        argVec.push_back(bufVec);
        bufVec = {(1 / 0.09), 1};
        argVec.push_back(bufVec);

        graph = new Graph(argVec);
        solutor = new TradeArbitrage(* graph);
    }
    void TearDown() {
        delete graph;
        delete solutor;
    }
};

TEST_F(UnitTest, checkSolvable_1) {
    ASSERT_EQ(solutor->findArbitrage(), "NO");
}

TEST_F(UnitTest, checkSolvable_2) {
    std::vector< std::vector<float> > argVec;
    std::vector<float> bufVec = {1, (1 / 32.1), (1 / 1.50), (1 / 78.66)};
    argVec.push_back(bufVec);
    bufVec = {(1 / 0.03), 1, (1 / 0.04), (1 / 2.43)};
    argVec.push_back(bufVec);
    bufVec = {(1 / 0.67), (1 / 21.22), 1, (1 / 51.89)};
    argVec.push_back(bufVec);
    bufVec = {(1 / 0.01), -1, (1 / 0.02), 1};
    argVec.push_back(bufVec);

    delete graph;
    delete solutor;
    graph = new Graph(argVec);
    solutor = new TradeArbitrage(* graph);
    ASSERT_EQ(solutor->findArbitrage(), "YES");
}

TEST_F(UnitTest, checkSolvable_3) {
    std::vector< std::vector<float> > argVec;
    std::vector<float> bufVec = {1, (1 / 0.67), -1};
    argVec.push_back(bufVec);
    bufVec = {-1, 1, (1 / 78.66),};
    argVec.push_back(bufVec);
    bufVec = {(1 / 0.02), -1, 1};
    argVec.push_back(bufVec);

    delete graph;
    delete solutor;
    graph = new Graph(argVec);
    solutor = new TradeArbitrage(* graph);
    ASSERT_EQ(solutor->findArbitrage(), "YES");
}

int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

