#pragma once
#include <vector>
#include <iostream>

float min(float a, float b);

class Graph {
    int nodeCount;
    std::vector< std::vector<float> > table;
public:
    Graph();
    Graph(std::vector< std::vector<float> > arg);
    Graph(const Graph& g);
    void initFromStream(std::istream& in);
    int getNodeCount();
    Graph& operator= (const Graph& g);
    friend class TradeArbitrage;
};

class TradeArbitrage {
    Graph *g;
public:
    TradeArbitrage(Graph& object);
    std::string findArbitrage();
};